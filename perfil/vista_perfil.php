

<form class="form-horizontal" id="edit_profile_form">
              <h5>
                Información de ingreso
              </h5>
              <hr>
              <div class="form-group">
                <label for="user_name" class="control-label">Nombre</label>
                <div class="">
                  <input type="text" name="user_name" id="user_name" class="form-control" value="<?php echo $name; ?>" required />
                  <i class="form-group__bar"></i>
                </div>
              </div>
              <div class="form-group">
                <label for="user_email" class="control-label">Email</label>
                <div class="">
                  <input type="email" name="user_email" id="user_email" class="form-control" required value="<?php echo $email; ?>" />
                  <i class="form-group__bar"></i>
                </div>
              </div>

              <br />
              <h5>
              <div class="text-center">
                 <h2>Deje la contraseña en blanco si no desea cambiarla</h2> 
              </div>
              </h5>
              <!-- <hr> -->
              <div class="form-group">
                <label for="user_new_password" class="control-label">Contraseña</label>
                <div class="">
                  <input type="password" name="user_new_password" id="user_new_password" class="form-control" />
                  <i class="form-group__bar"></i>
                </div>
              </div>
              <div class="form-group">
                <label for="user_re_enter_password" class="control-label">Confirmar Contraseña</label>
                <div class="">
                  <input type="password" name="user_re_enter_password" id="user_re_enter_password" class="form-control" />
                  <i class="form-group__bar"></i>
                  <input type="hidden" name="user_id2" id="user_id2" value="<?php echo $user_id; ?>" class="form-control" />
                  <span  id="error_password"></span>
                </div>
              </div>

              <div class="form-actions">
                <button style="cursor:pointer;" type="submit" name="edit_prfile" id="edit_prfile" class="btn btn-light pull-right">
                  Guardar cambios
                </button>
              </div>
            </form>