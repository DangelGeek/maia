ffmpeg version 2.8.14-0ubuntu0.16.04.1 Copyright (c) 2000-2018 the FFmpeg developers
  built with gcc 5.4.0 (Ubuntu 5.4.0-6ubuntu1~16.04.9) 20160609
  configuration: --prefix=/usr --extra-version=0ubuntu0.16.04.1 --build-suffix=-ffmpeg --toolchain=hardened --libdir=/usr/lib/x86_64-linux-gnu --incdir=/usr/include/x86_64-linux-gnu --cc=cc --cxx=g++ --enable-gpl --enable-shared --disable-stripping --disable-decoder=libopenjpeg --disable-decoder=libschroedinger --enable-avresample --enable-avisynth --enable-gnutls --enable-ladspa --enable-libass --enable-libbluray --enable-libbs2b --enable-libcaca --enable-libcdio --enable-libflite --enable-libfontconfig --enable-libfreetype --enable-libfribidi --enable-libgme --enable-libgsm --enable-libmodplug --enable-libmp3lame --enable-libopenjpeg --enable-libopus --enable-libpulse --enable-librtmp --enable-libschroedinger --enable-libshine --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libssh --enable-libtheora --enable-libtwolame --enable-libvorbis --enable-libvpx --enable-libwavpack --enable-libwebp --enable-libx265 --enable-libxvid --enable-libzvbi --enable-openal --enable-opengl --enable-x11grab --enable-libdc1394 --enable-libiec61883 --enable-libzmq --enable-frei0r --enable-libx264 --enable-libopencv
  libavutil      54. 31.100 / 54. 31.100
  libavcodec     56. 60.100 / 56. 60.100
  libavformat    56. 40.101 / 56. 40.101
  libavdevice    56.  4.100 / 56.  4.100
  libavfilter     5. 40.101 /  5. 40.101
  libavresample   2.  1.  0 /  2.  1.  0
  libswscale      3.  1.101 /  3.  1.101
  libswresample   1.  2.101 /  1.  2.101
  libpostproc    53.  3.100 / 53.  3.100
Input #0, wav, from 'test3.wav':
  Duration: 00:09:12.46, bitrate: 13 kb/s
    Stream #0:0: Audio: gsm_ms (1[0][0][0] / 0x0031), 8000 Hz, mono, s16, 13 kb/s
Output #0, null, to 'pipe:':
  Metadata:
    encoder         : Lavf56.40.101
    Stream #0:0: Audio: pcm_s16le, 8000 Hz, mono, s16, 128 kb/s
    Metadata:
      encoder         : Lavc56.60.100 pcm_s16le
Stream mapping:
  Stream #0:0 -> #0:0 (gsm_ms (native) -> pcm_s16le (native))
Press [q] to stop, [?] for help
[silencedetect @ 0x2556300] silence_start: 0.18
[silencedetect @ 0x2556300] silence_end: 7.8 | silence_duration: 7.62
[silencedetect @ 0x2556300] silence_start: 12.02
[silencedetect @ 0x2556300] silence_end: 13.08 | silence_duration: 1.06
[silencedetect @ 0x2556300] silence_start: 13.22
[silencedetect @ 0x2556300] silence_end: 13.8 | silence_duration: 0.58
[silencedetect @ 0x2556300] silence_start: 14.34
[silencedetect @ 0x2556300] silence_end: 15.68 | silence_duration: 1.34
[silencedetect @ 0x2556300] silence_start: 41.86
[silencedetect @ 0x2556300] silence_end: 42.44 | silence_duration: 0.58
[silencedetect @ 0x2556300] silence_start: 42.46
[silencedetect @ 0x2556300] silence_end: 43.76 | silence_duration: 1.3
[silencedetect @ 0x2556300] silence_start: 48.26
[silencedetect @ 0x2556300] silence_end: 49.16 | silence_duration: 0.9
[silencedetect @ 0x2556300] silence_start: 51.14
[silencedetect @ 0x2556300] silence_end: 51.92 | silence_duration: 0.78
[silencedetect @ 0x2556300] silence_start: 52.82
[silencedetect @ 0x2556300] silence_end: 53.36 | silence_duration: 0.54
[silencedetect @ 0x2556300] silence_start: 54.98
[silencedetect @ 0x2556300] silence_end: 56.76 | silence_duration: 1.78
[silencedetect @ 0x2556300] silence_start: 61.26
[silencedetect @ 0x2556300] silence_end: 62.24 | silence_duration: 0.98
[silencedetect @ 0x2556300] silence_start: 66.58
[silencedetect @ 0x2556300] silence_end: 67.72 | silence_duration: 1.14
[silencedetect @ 0x2556300] silence_start: 67.74
[silencedetect @ 0x2556300] silence_end: 156.76 | silence_duration: 89.02
[silencedetect @ 0x2556300] silence_start: 185.26
[silencedetect @ 0x2556300] silence_end: 185.76 | silence_duration: 0.5
[silencedetect @ 0x2556300] silence_start: 188.86
[silencedetect @ 0x2556300] silence_end: 189.4 | silence_duration: 0.54
[silencedetect @ 0x2556300] silence_start: 193.66
[silencedetect @ 0x2556300] silence_end: 194.6 | silence_duration: 0.94
[silencedetect @ 0x2556300] silence_start: 195.1
[silencedetect @ 0x2556300] silence_end: 197.12 | silence_duration: 2.02
[silencedetect @ 0x2556300] silence_start: 197.1
[silencedetect @ 0x2556300] silence_end: 198.44 | silence_duration: 1.34
[silencedetect @ 0x2556300] silence_start: 200.06
[silencedetect @ 0x2556300] silence_end: 201.08 | silence_duration: 1.02
[silencedetect @ 0x2556300] silence_start: 201.62
[silencedetect @ 0x2556300] silence_end: 203.48 | silence_duration: 1.86
[silencedetect @ 0x2556300] silence_start: 203.54
[silencedetect @ 0x2556300] silence_end: 205.12 | silence_duration: 1.58
[silencedetect @ 0x2556300] silence_start: 205.18
[silencedetect @ 0x2556300] silence_end: 208.76 | silence_duration: 3.58
[silencedetect @ 0x2556300] silence_start: 208.78
[silencedetect @ 0x2556300] silence_end: 326.76 | silence_duration: 117.98
[silencedetect @ 0x2556300] silence_start: 326.78
[silencedetect @ 0x2556300] silence_end: 345.68 | silence_duration: 18.9
[silencedetect @ 0x2556300] silence_start: 348.22
[silencedetect @ 0x2556300] silence_end: 348.76 | silence_duration: 0.54
[silencedetect @ 0x2556300] silence_start: 348.9
[silencedetect @ 0x2556300] silence_end: 350 | silence_duration: 1.1
[silencedetect @ 0x2556300] silence_start: 350.34
[silencedetect @ 0x2556300] silence_end: 404.24 | silence_duration: 53.9
[silencedetect @ 0x2556300] silence_start: 404.22
[silencedetect @ 0x2556300] silence_end: 412.36 | silence_duration: 8.14
[silencedetect @ 0x2556300] silence_start: 412.34
[silencedetect @ 0x2556300] silence_end: 415.12 | silence_duration: 2.78
[silencedetect @ 0x2556300] silence_start: 415.14
[silencedetect @ 0x2556300] silence_end: 420.92 | silence_duration: 5.78
[silencedetect @ 0x2556300] silence_start: 420.94
[silencedetect @ 0x2556300] silence_end: 429.8 | silence_duration: 8.86
[silencedetect @ 0x2556300] silence_start: 429.82
[silencedetect @ 0x2556300] silence_end: 448.56 | silence_duration: 18.74
[silencedetect @ 0x2556300] silence_start: 448.58
[silencedetect @ 0x2556300] silence_end: 460.24 | silence_duration: 11.66
[silencedetect @ 0x2556300] silence_start: 481.1
[silencedetect @ 0x2556300] silence_end: 482 | silence_duration: 0.9
[silencedetect @ 0x2556300] silence_start: 500.02
[silencedetect @ 0x2556300] silence_end: 500.92 | silence_duration: 0.9
[silencedetect @ 0x2556300] silence_start: 504.02
[silencedetect @ 0x2556300] silence_end: 505.2 | silence_duration: 1.18
[silencedetect @ 0x2556300] silence_start: 505.5
[silencedetect @ 0x2556300] silence_end: 507.64 | silence_duration: 2.14
[silencedetect @ 0x2556300] silence_start: 508.06
[silencedetect @ 0x2556300] silence_end: 508.72 | silence_duration: 0.66
[silencedetect @ 0x2556300] silence_start: 508.82
[silencedetect @ 0x2556300] silence_end: 511.76 | silence_duration: 2.94
[silencedetect @ 0x2556300] silence_start: 512.42
[silencedetect @ 0x2556300] silence_end: 513.2 | silence_duration: 0.78
[silencedetect @ 0x2556300] silence_start: 514.06
[silencedetect @ 0x2556300] silence_end: 514.76 | silence_duration: 0.7
[silencedetect @ 0x2556300] silence_start: 516.3
[silencedetect @ 0x2556300] silence_end: 517.44 | silence_duration: 1.14
[silencedetect @ 0x2556300] silence_start: 518.3
[silencedetect @ 0x2556300] silence_end: 518.88 | silence_duration: 0.58
[silencedetect @ 0x2556300] silence_start: 519.1
[silencedetect @ 0x2556300] silence_end: 520.88 | silence_duration: 1.78
[silencedetect @ 0x2556300] silence_start: 524.66
[silencedetect @ 0x2556300] silence_end: 525.36 | silence_duration: 0.7
[silencedetect @ 0x2556300] silence_start: 541.66
[silencedetect @ 0x2556300] silence_end: 542.56 | silence_duration: 0.9
[silencedetect @ 0x2556300] silence_start: 551.5
[silencedetect @ 0x2556300] silence_end: 552.16 | silence_duration: 0.66
size=N/A time=00:09:12.48 bitrate=N/A    
video:0kB audio:8632kB subtitle:0kB other streams:0kB global headers:0kB muxing overhead: unknown
