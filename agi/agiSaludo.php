#!/usr/bin/php -q
<?php
require("/var/www/html/AGI/phpagi.php");
$mysqli = new mysqli("localhost", "root", "m9a7r5s3", "maia");
if (mysqli_connect_errno()) {
    printf("Error de conexión: %s\n", mysqli_connect_error());
    exit();
}
$buscarGrabacion = $argv[1];
$nombreCompleto = $argv[2];
$exten = $argv[3];
$contador = $argv[4];

$agi = new AGI();


if($contador==0){
    $horaServer = date("G:H:s");
    $dia = "";
    $tarde = "";
    $noche = "";

    $sqlHora = $mysqli->query("SELECT dia,tarde,noche FROM BT_horario");
    foreach($sqlHora as $rowHora){
        $dia = $rowHora['dia'];
        $tarde = $rowHora['tarde'];
        $noche = $rowHora['noche'];
    }

    if($horaServer>=$dia){
        $agi->exec(Playback,"/var/www/html/AGI/FRASES/J_buenastardeshablocon");
    }
    elseif($horaServer>=$tarde){
        $agi->exec(Playback,"/var/www/html/AGI/FRASES/J_buenastardeshablocon");
    }
    elseif($horaServer>=$noches){
    }

    $arrayNombre = explode(" ", $nombreCompleto);
    $nombre = $arrayNombre[0];
    $nombreLetra = strtoupper(substr("$nombre", 0 ,1)); 
    $apellido = $arrayNombre[1];
    $apellidoLetra = strtoupper(substr("$apellido", 0 ,1)); 

    $rutaNombre = "/var/www/html/AGI/NOMBRES/".$nombreLetra."/".$nombre;
    $rutaApellido = "/var/www/html/AGI/APELLIDOS/".$apellidoLetra."/".$apellido;
    if($buscarGrabacion==1){
        $agi->exec(Playback,"$rutaNombre");
        $agi->exec(Playback,"$rutaApellido");
        $agi->set_variable(nombreUnico,"$rutaNombre");
        $agi->set_variable(contadorMuletilla,1);

        $agi->exec_goto($exten,10);
    }else{
        $agi->exec_goto($exten,8);
    }
}elseif($contador==1){
    $arrayNombre = explode(" ", $nombreCompleto);
    $nombre = $arrayNombre[0];
    $nombreLetra = strtoupper(substr("$nombre", 0 ,1)); 
    $apellido = $arrayNombre[1];
    $apellidoLetra = strtoupper(substr("$apellido", 0 ,1)); 

    $rutaNombre = "/var/www/html/AGI/NOMBRES/".$nombreLetra."/".$nombre;
    $rutaApellido = "/var/www/html/AGI/APELLIDOS/".$apellidoLetra."/".$apellido;
    if($buscarGrabacion==1){
        $agi->exec(Playback,"/var/www/html/AGI/FRASES/J_disculpe");
        $agi->exec(Playback,"$rutaNombre");
        $agi->exec(Playback,"$rutaApellido");
        $agi->set_variable(nombreUnico,"$rutaNombre");
        $agi->set_variable(contadorMuletilla,2);
        $agi->exec_goto($exten,10);
    }else{
        $agi->exec(Playback,"/var/www/html/AGI/FRASES/J_disculpe");
        $agi->exec_goto($exten,8);
    }
}elseif($contador==2){
    $arrayNombre = explode(" ", $nombreCompleto);
    $nombre = $arrayNombre[0];
    $nombreLetra = strtoupper(substr("$nombre", 0 ,1)); 
    $apellido = $arrayNombre[1];
    $apellidoLetra = strtoupper(substr("$apellido", 0 ,1)); 

    $rutaNombre = "/var/www/html/AGI/NOMBRES/".$nombreLetra."/".$nombre;
    $rutaApellido = "/var/www/html/AGI/APELLIDOS/".$apellidoLetra."/".$apellido;
    if($buscarGrabacion==1){
        $agi->exec(Playback,"$rutaNombre");
        $agi->exec(Playback,"$rutaApellido");
        $agi->set_variable(nombreUnico,"$rutaNombre");
        $agi->set_variable(contadorMuletilla,2);
        $agi->exec_goto($exten,10);
    }else{
        $agi->exec_goto($exten,8);
    }
}

?>