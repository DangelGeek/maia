<?php

include('../includes/login/login.php');


if(!isset($_SESSION["type"]))
{
 header("location:../");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Vendor styles -->
        <link rel="stylesheet" href="../vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="../vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="../vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        
        <!-- App styles -->
        <link rel="stylesheet" href="../css/app2.css">
        <!-- botones de la tabla bot al darles play -->
        <link rel="stylesheet" href="../css/estados.css">
        <!-- <link rel="stylesheet" href="css/app.min.css"> -->
    </head>

        <body data-sa-theme="1">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>

             <?php
            include '../plantillas/header.php';
            include '../plantillas/aside.php';
            ?>

            <div class="themes">
                <div class="scrollbar-inner">
                </div>
            </div>


            <section class="content">
                <header class="content__title">
                    <h1>Configuración de Cuenta</h1>

                </header>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">CUENTAS</h4>
                        <!-- <h6 class="card-subtitle">DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table.</h6> -->

                        <div class="table-responsive">
                            <table id="user_data" class="table table-hover table-bordered table-sm mb-0">
                                <thead>
                                    <tr>
                                    <th readonly>ID</th>
                                    <th>Email</th>
                                    <th>Nombre</th>
                                    <th>Tipo</th>
                                    <th>Estatus</th>
                                    <th>Editar</th>
                                    <th style="width: 167px;">Cambiar Estatus</th>
                                    <th style="width: 147px;">Eliminar</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                
                
                <?php
                    include '../plantillas/modal.php';
                    include '../plantillas/footer.php'
                ?>
            </section>
        </main>

        <!-- Javascript -->
        <!-- Vendors -->
        <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="../vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>
        <script src="../vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
        <!-- Vendors: Data tables -->
        <script src="../vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src='../js/edit_perfil/edit_perfil.js'></script>
        
        <!-- App functions and actions -->
        <script src="../js/app.min.js"></script>
        <script>
        $(document).ready(function(){
            $('#configuracion').addClass('navigation__sub--active');
            $('#configuracion_perfil').addClass('navigation__active');
        });
        </script>
        <!--jQuery validate para validar formulario en el front-end-->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script> -->
        
    </body>
</htmls