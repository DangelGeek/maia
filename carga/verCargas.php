<?php

include('../includes/login/login.php');


if(!isset($_SESSION["type"]))
{
 header("location:../");
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Vendor styles -->
        <link rel="stylesheet" href="../vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="../vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="../vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        
        <link rel="stylesheet" href="../vendors/bower_components/select2/dist/css/select2.min.css">
        <link rel="stylesheet" href="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.css">
        
        
        <!-- App styles -->
        <link rel="stylesheet" href="../css/app2.css">

        <!-- Demo only -->
        <!-- <link rel="stylesheet" href="../demo/css/demo.css"> -->
    </head>

    <body data-sa-theme="1">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>

    <?php
    include '../plantillas/header.php';
    include '../plantillas/aside.php';
    ?>
    <div class="themes">
    <div class="scrollbar-inner">
        
    </div>
</div>


            <section class="content">
            <div class="content__inner">
                    <header class="content__title">
                        
                    </header>


                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Consulta de Cargas</h4>


                            <div class="form-group">
                                <label>Seleccione Nombre de la Carga</label>
                                <select class="select2" id="select_nombre">
                                </select>
                            </div>

                            <br>
                            <a class="btn btn-light"  id="consultar" href="#" role="button">Consultar</a>
                            
                        </div>
                    </div>
                </div>
                <div class="content__inner">
                    <header class="content__title">
                        <!-- <h1>Descargar Reporte</h1> -->
                        
                    </header>

                    
                    <div  id="tabla_cargas" class=" card">
                        <div class="card-body">
                            <h4 class="card-title">Tabla de Cargas</h4>

                            
                            <div class="table-responsive" id="">
                            <table id="cargas_data" class="table table-hover table-bordered table-sm mb-0">
                                <thead>
                                    <tr>
                                        <th readonly>ID</th>
                                        <th>Cedente</th>
                                        <th>Rut</th>
                                        <th>Nombre</th>
                                        <th>Fono</th>
                                        <th>Id Carga</th>
                                        <th>Discador</th>
                                        <!-- <th>Editar</th> -->
                                        <!-- <th style="width: 167px;">Cambiar Estatus</th> -->
                                        <!-- <th>Eliminar</th> -->
                                    </tr>
                                </thead>
                            </table>
                            </div>
                            
                            <!-- <br>
                            <div class="" id="mensaje"></div>
                                <form id="form_reporte" method"post" action="../includes/reporte/excel.php">
                                    <input style="cursor:pointer;" type="submit" class="btn btn-light" name="exportar_reporte" value="Descargar Reporte">
                                    <input type="hidden" name="exportar_reporte" value="exportar_reporte">
                                </form> -->
                               
                        </div>
                    </div>
                
                
            </div>

                <?php
                    include '../plantillas/footer.php'
                ?>
            </section>
        </main>

         <!-- Javascript -->
        <!-- Vendors -->
        <script src="../vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="../vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="../vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>
        <script src="../vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="../vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
        <!-- Vendors: Data tables -->
        <script src="../vendors/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
        <script src="../vendors/bower_components/jszip/dist/jszip.min.js"></script>
        <script src="../vendors/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
        <script src="../vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
        <script src='../js/socket/socket.io.js'></script>
        <script src='../js/global/global.js'></script>

         <script src='../js/carga/ver_cargas.js'></script>
        
       
        <!-- App functions and actions -->
        <script src="../js/app.min.js"></script>
        <script>
        $(document).ready(function(){
            $('#carga').addClass('navigation__sub--active');
            
            $('#carga_ver').addClass('navigation__active');
        });
        </script>
        <!--jQuery validate para validar formulario en el front-end-->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script> -->
    </body>
</html>