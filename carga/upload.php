<?php
include("../class/db/db.php");
$targetFile = "";
if(!empty($_FILES)){
    
    $targetDir = "../file/post/";

    $file_array = explode(".", $_FILES['file']['name']);  
    if($file_array[1] == "xls" || $file_array[1] == "xlsx")  
    {
        $fileName = $_FILES['file']['name'];
        $targetFile = $targetDir.$fileName;
    
        if(move_uploaded_file($_FILES['file']['tmp_name'],$targetFile)){
            $db = new db("maia");
            $sql = "INSERT  INTO CRG_file(nombre) VALUES ('".$targetFile."')";
            $rows = $db->query($sql);
            
            if(isset($rows) && $rows > 0)
            {
                $mensaje = 'Exito';
                $texto = 'Datos insertados';
                $tipo = 'success';
            }
            else
            {
                $mensaje = 'Error!';
                $texto = 'Los campos que se subieron no son correctos, descargue el archivo de ejemplo';
                $tipo = 'warning';
            }
            
        }
    }
    else
    {
        $mensaje = 'Error!';
        $texto = 'Extensión invalida';
        $tipo = 'warning';
    }

    $data = array(
        'mensaje' => $mensaje,
        'tipo'    => $tipo,
        'texto'   => $texto
    );
    echo  json_encode($data);
    
}

?>