<?php


class gestion {
    private $db;
    private $session;
    public function __construct() {
        $this->db=Conectar::conexion();
    }

    // metodo para descargar excel
    public function getExcel($exportar) {
        $output = '';
        if(isset($exportar))
        {
        $query = "SELECT id, nombre, fecha, ruts, telefonos, dialplan, codigo, defecto FROM CRG_config ";

        $result = mysqli_query($this->db, $query);
        if(mysqli_num_rows($result) > 0)
        {
        $output .= '
        <table class="table" bordered="1">  
                            <tr>  
                                <th>ID</th>  
                                <th>Nombre</th>  
                                <th>Fecha</th>
                                <th>Ruts</th>
                                <th>Telefonos</th>
                                <th>Dialplan</th>
                                <th>codigo</th>
                                <th>defecto</th>
                            </tr>
        ';
        while($row = mysqli_fetch_array($result))
        {
        $output .= '
            <tr>  
                <td>'.$row["id"].'</td>  
                <td>'.$row["nombre"].'</td>  
                <td>'.$row["fecha"].'</td>  
                <td>'.$row["ruts"].'</td>  
                <td>'.$row["telefonos"].'</td>
                <td>'.$row["dialplan"].'</td>
                <td>'.$row["codigo"].'</td>
                <td>'.$row["defecto"].'</td>
            </tr>';
        }
        $output .= '</table>';
        header('Content-Type: application/xls');
        header('Content-Disposition: attachment; filename=reporte_bot.xls');
        echo $output;
        }
        }
        else
        {
            echo 'error';
        }
    }
    public function stop() {

        if($_POST['btn_action'] == 'stop' && $_POST['status'] != 0){
        $status = 0;
        $tabla = "";
        $query_estatus = " update BT_config set estado = '".$status."' where id = '".$_POST["user_id"]."' ";
        $result = $this->db->query($query_estatus);

        
        if(isset($result) && $result > 0){
            
            echo 'Estado Cambiado a Stop';
            $queryTabla= " SELECT tabla FROM  BT_config WHERE  id = '".$_POST["user_id"]."' ";
            $resultTabla = $this->db->query($queryTabla);
            foreach($resultTabla as $row){
                $tabla = $row['tabla'];
                $updateTabla = "UPDATE $tabla SET llamado = 0"; 
                $this->db->query($updateTabla);
            }
            }
        }
        
    }
    public function  controles() {
        
        // condicion para eliminar la fila seleccionada
        if($_POST['btn_action'] == 'borrar'){
        
            //selecciono el nombre de la tabla para borrarla
            $query_tabla = " select tabla from BT_config where id = '".$_POST["user_id"]."'";
            $result_tabla = $this->db->query($query_tabla);
            $fila_tabla = $result_tabla->fetch_all(MYSQLI_ASSOC);
            foreach($fila_tabla as $tabla)
            $tabla_borrada = $tabla['tabla'];
            
            //borro la tabla
            $query_drop_tabla = "drop table $tabla_borrada";
            $result_drop = $this->db->query($query_drop_tabla);
            //si se borra la tabla
            if(isset($result_drop) && $result_drop > 0){
                //borro el registro en BT_config
                $query_delete = "
                delete  from BT_config
                where id = '".$_POST["user_id"]."'
                ";
                $result = $this->db->query($query_delete);
                //si borro el registro
                if(isset($result) && $result > 0){
                    
                    echo 'Datos Eliminados ';
                }
            }
            else
            {
                echo 'No se pudo Eliminar la tabla';
            }
        }

        //cambiar estatus de controles play, pause
        if($_POST['btn_action'] == 'play' && $_POST['status'] != 1){
            $status = 1;
            $query_estatus = "
                update BT_config
                set estado = '".$status."'
                where id = '".$_POST["user_id"]."'
            ";
            $id = $_POST["user_id"];
            $result = $this->db->query($query_estatus);
            if(isset($result)){
                shell_exec("sudo php /var/www/html/includes/bot/sendBOT.php $id > /dev/null 2>&1 &");
                echo 'Estatus Cambiado a Play';
            }
        }
        elseif($_POST['btn_action'] == 'pause' && $_POST['status'] != 2){
            $status = 2;
            $query_estatus = "
                update BT_config
                set estado = '".$status."'
                where id = '".$_POST["user_id"]."'
            ";
            $result = $this->db->query($query_estatus);
            if(isset($result)){
                
                echo 'Estado Cambiado a Pause';
            }
        }
        
        
    }

    // metodo para obtener todos las cargas creadas
    public function getCargas() {
        
        $this->session=session_start();
        
        
        $query = '';

        $output = array();
        $query .= "SELECT * FROM BT_config ";
        if(isset($_POST["search"]["value"]))
        {
            $query .= 'where id LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR nombre LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR cantidad_fono LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'OR cantidad_rut LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR tabla LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR estado LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR ruts LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR telefonos LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR canales LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR dialplan LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR codigo LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR defecto LIKE "%'.$_POST["search"]["value"].'%" ';
        }
        if(isset($_POST["order"]))
        {
            $query .= 'ORDER by '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
        }
        else{
            $query .= 'ORDER BY id DESC ';
        }
        if($_POST["length"] != -1)
        {
            $query .= 'LIMIT ' .$_POST['start'] . ', ' . $_POST['length'];
        }


        //ejecutamos la consulta
        $consulta = $this->db->query($query);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        $data = array();
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $filtered_rows = count($respuesta);
        
        foreach($respuesta as $row)
        {
            $tabla = $row['tabla'];
            $queri_tablas = '';
            $consulta_tablas = '';
            $respuestatablas  = '';
            $filtered_rows_tablas = '';
            
            $sql = "SELECT COUNT(*) Progreso FROM $tabla WHERE llamado = 1";
            $result = $this->db->query($sql);
            $fila = $result->fetch_all(MYSQLI_ASSOC);
            foreach($fila as $fila_total)
            $fila_total['Progreso'];

            $sql2 = "SELECT COUNT(*) Total_rut FROM $tabla where Rut is not null and length(Rut) > 0";
            $result2 = $this->db->query($sql2);
            $fila2 = $result2->fetch_all(MYSQLI_ASSOC);
            foreach($fila2 as $fila_ruts)
            $fila_ruts['Total_rut'];
            
            
            $sql3 = "SELECT COUNT(*) Total_fonos FROM $tabla where Fono is not null and length(Fono) > 0";
            $result3 = $this->db->query($sql3);
            $fila3 = $result3->fetch_all(MYSQLI_ASSOC);
            foreach($fila3 as $fila_ponos)
            $fila_ponos['Total_fonos'];

            $play = '';
            $pause = '';
            $stop = '';
            if($row['estado'] == 0)
            {
                $play;
                $pause;
                $stop = 'stop';
            }
            if($row['estado'] == 1)
            {
                $play = 'play';
                $pause;
                $stop;
            }
            if($row['estado'] == 2)
            {
                $play;
                $pause = 'pause';
                $stop;
            }
            
            $sub_array = array();
            $sub_array[] = $row['id'];
            $sub_array[] = $row['nombre'];
            $sub_array[] = $row['tabla'];
            $sub_array[] = $fila_total['Progreso'].'/'.$fila_ruts['Total_rut'];
            $sub_array[] = $fila_total['Progreso'].'/'.$fila_ponos['Total_fonos'];
            $tipo_usuario = $_SESSION['type'];
            $id_usuario = $_SESSION['id'];
            if($tipo_usuario == 'user' && $id_usuario != $row['id_usuario'])
            {
                $sub_array[] = '<button style="cursor:pointer; font-size:18px;" type="button" name="play" id="'.$row["id"].'" class="btn btn-success btn-xs cambiar 1 '.$play.'"  data-status="'.$row["estado"].'" disabled><i class="zmdi zmdi-play-circle"></i></button>';
                $sub_array[] = '<button style="cursor:pointer; font-size:18px;" type="button" name="pause" id="'.$row["id"].'" class="btn btn-warning btn-xs cambiar 2 '.$pause.'"  data-status="'.$row["estado"].'" disabled><i class="zmdi zmdi-pause"></i></button>';
                $sub_array[] = '<button style="cursor:pointer; font-size:18px;" type="button" name="stop" id="'.$row["id"].'" class="btn btn-danger btn-xs cambiar 0 '.$stop.'" data-status="'.$row["estado"].'" disabled><i class="zmdi zmdi-stop"></i></button>';
                $sub_array[] = '<button style="cursor:pointer; font-size:18px;" type="button" name="borrar" id="'.$row["id"].'" class="btn btn-dark btn-xs borrar" disabled><i class="zmdi zmdi-delete"></i></button>';
            }
            else
            {
                $sub_array[] = '<button style="cursor:pointer; font-size:18px;" type="button" name="play" id="'.$row["id"].'" class="btn btn-success btn-xs cambiar 1 '.$play.'"  data-status="'.$row["estado"].'"><i class="zmdi zmdi-play-circle"></i></button>';
                $sub_array[] = '<button style="cursor:pointer; font-size:18px;" type="button" name="pause" id="'.$row["id"].'" class="btn btn-warning btn-xs cambiar 2 '.$pause.'"  data-status="'.$row["estado"].'"><i class="zmdi zmdi-pause"></i></button>';
                $sub_array[] = '<button style="cursor:pointer; font-size:18px;" type="button" name="stop" id="'.$row["id"].'" class="btn btn-danger btn-xs cambiar 0 '.$stop.'" data-status="'.$row["estado"].'"><i class="zmdi zmdi-stop"></i></button>';
                $sub_array[] = '<button style="cursor:pointer; font-size:18px;" type="button" name="borrar" id="'.$row["id"].'" class="btn btn-dark btn-xs borrar"><i class="zmdi zmdi-delete"></i></button>';
            }
            
            
            $data[]      = $sub_array;
        }
        $registro = new Registros();
        $tabla = 'BT_config';
        $output = array(
            "draw"              => intval($_POST["draw"]),
            "recordsTotal"      => $filtered_rows,
            "recordsFiltered"   => $registro->getTotalRegistros($tabla),
            "data"              => $data
        );

        return json_encode($output);
    }
    // fin metodo para obtener todas las cargas

}

?>