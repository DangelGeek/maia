<?php
class carga{   

    private $db;

    //obtener la carga en datatable de la carga excel seleccionada
    public function VerCargas () {

        $this->db=Conectar::conexion();

        $fp = fopen("bt_id.txt", "r");
        $btTabla = fgets($fp);
        
        fclose($fp);
        
        $query = '';

        $output = array();

        $query .= " SELECT * FROM $btTabla WHERE id >= 0 ";
        // $query .= " SELECT * FROM RP_resultado_bot ";
        if(isset($_POST["search"]["value"]))
        {
            // $query .= 'OR id LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= ' OR Rut LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= ' OR Fono LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'and Rut LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR fecha LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR hora LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR urlGrabacion LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR idCarga LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR desvio LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR respuesta_cliente LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR rut2 LIKE "%'.$_POST["search"]["value"].'%" ';
        }
        if(isset($_POST["order"]))
        {
            $query .= 'ORDER by '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
        }
        else{
            $query .= 'ORDER BY id DESC ';
        }
        if($_POST["length"] != -1)
        {
            $query .= 'LIMIT ' .$_POST['start'] . ', ' . $_POST['length'];
        }

        
        //ejecutamos la consulta
        $consulta = $this->db->query($query);
        
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);

        
        $data = array();
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $filtered_rows = count($respuesta);
        
        foreach($respuesta as $row)
        {
            
            $sub_array = array();
            $sub_array[] = $row['id'];
            $sub_array[] = $row['cedente'];
            $sub_array[] = $row['Rut'];
            $sub_array[] = $row['Nombre_Completo'];
            $sub_array[] = $row['Fono'];
            $sub_array[] = $row['idCarga'];
            if($row['llamado'] == 1)
            $sub_array[] = 'Si';
            else
            $sub_array[] = 'No';
            // $sub_array[] = $row['rut2'];
            // $sub_array[] = '<button style="cursor:pointer;" type="button" name="update" id="'.$row["id"].'" class="btn btn-warning btn-xs update">Actualizar</button>';
            // $sub_array[] = '<button type="button" name="cambiar" id="'.$row["id"].'" class="btn btn-danger btn-xs cambiar" data-status="'.$row["cliente"].'">Cambiar Estatus</button>';
            // $sub_array[] = '<button style="cursor:pointer;" type="button" name="borrar" id="'.$row["id"].'" class="btn btn-danger btn-xs borrar">Eliminar Reporte</button>';
            $data[]      = $sub_array;
        }
        $registro = new Registros();
        $tabla = $btTabla;
        $output = array(
            "draw"              => intval($_POST["draw"]),
            "recordsTotal"      => $filtered_rows,
            "recordsFiltered"   => $registro->getTotalRegistros($tabla),
            "data"              => $data
        );

        return json_encode($output);
    }
    
    //metodo para guardar el nombre de la tabla a consultar y mostrar luego sus datos
    public function GuardarTabla(){
        
        $bt_id = $_POST['bt_id'];
        $fp = fopen("bt_id.txt", "w");
        fputs($fp, $bt_id);
        fclose($fp);
    }
    // funcion para obtener todos los nombres y tablas en Ver Cargas
    public function getNombreCarga () {
        
        $db = new db("maia");
        $descripciones = '';
        $sql = "SELECT tabla, nombre FROM BT_config order by id desc";
            //ejecutamos la consulta
            $result = $db->query($sql);
            $cantidad = $result->num_rows;
            echo $cantidad;
            if($cantidad > 0){
                $descripciones .= '<option value="Seleccione">Seleccione</option>';
                foreach($result as $rowsD){
                    
                    $descripciones .= '<option class="id" id="'.$rowsD["tabla"].'" value="'.$rowsD["tabla"].'">'.$rowsD['nombre'].'</option>';
                }
                
            }else{
                $descripciones = '<option value="">No existe data</option>';
            }
            return $descripciones;
    }

    public function descripcion () {
        $db = new db("maia");
        $descripciones = '';
        $sql = "SELECT id, descripcion FROM BT";
            $result = $db->query($sql);
            $cantidad = $result->num_rows;
            if($cantidad > 0){
                $descripciones .= '<option value="Seleccione">Seleccione</option>';
                foreach($result as $rowsD){
                    
                    $descripciones .= '<option class="id" id="'.$rowsD["id"].'" value="'.$rowsD["id"].'">'.$rowsD['descripcion'].'</option>';
                }
                
            }else{
                $descripciones = '<option value="">No existe data</option>';
            }
            return $descripciones;
    }

    public function subir($lastId, $servicio, $id_usuario){
        $db = new db("maia");
        if($servicio==1){
            $t = array();
            $fecha = date("Ymd");
            $hora= date("His");   

            $tableNombre = "BOT_".$fecha."_".$hora."_".$lastId;
            $dropTabla = "DROP TABLE IF EXISTS $tableNombre";
            $drop = $db->query($dropTabla);
            
            $crearTabla = "CREATE TABLE $tableNombre (id INT NOT NULL AUTO_INCREMENT,cedente VARCHAR(20),Rut VARCHAR(20),Nombre_Completo VARCHAR(100),Fono INT,idCarga INT,llamado INT NOT NULL DEFAULT '0', id_usuario INT, PRIMARY KEY (id)) ENGINE = MYISAM";
            $crear = $db->query($crearTabla);

            $sqlUpdate = "UPDATE BT_config SET tabla  = '".$tableNombre."' WHERE id = '".$lastId."'";
            $rowsUpdate= $db->query($sqlUpdate);
            // echo "ok";

            $targetFile = "";
            // $ruta = "../";
            $ruta = "";
            $file = "";
            $idFile = "";
            $sqlFile = "SELECT id,nombre FROM CRG_file WHERE process = 0 ORDER BY id DESC LIMIT 1";
            $result = $db->query($sqlFile);
            $cantidad = $result->num_rows;
           
            if($cantidad==1){
                foreach($result as $rowsF){
                    $file = $rowsF['nombre'];
                    $idFile = $rowsF['id'];
                    $targetFile = $ruta.$file;
                }
                
                $xlsx = new SimpleXLSX( "$targetFile" );
                if($xlsx->success())
                {
                    $fp = fopen( 'datos.csv', 'w');//Abrire un archivo "datos.csv", sino existe se creara
                
                    $i=0;
                    
                    foreach( $xlsx->rows() as $fields ) {//Itero la hoja de calculo
                        fputcsv( $fp, $fields);//Doy formato CSV a una línea y le escribo los datos
                        if($i!=0){
                            $cedente = $fields[0];
                            $rut = $fields[1];
                            $nombre = $fields[2];
                            $t1 = $fields[3];
                            $v1 = strlen($t1);
                            if($v1==9){
                                array_push($t, $t1);
                            }
                            
                            // $t2 = $fields[4];
                            // $v2 = strlen($t2);
                            // if($v2==9){
                            //     array_push($t, $t2);
                            // }
                            // $t3 = $fields[5];
                            // $v3 = strlen($t3);
                            // if($v3==9){
                            //     array_push($t, $t3);
                            // }
                            // $t4 = $fields[6];
                            // $v4 = strlen($t4);
                            // if($v4==9){
                            //     array_push($t, $t4);
                            // }
                            // $t5 = $fields[7];
                            // $v5 = strlen($t5);
                            // if($v5==9){
                            //     array_push($t, $t5);
                            // }
                            // $t6 = $fields[8];
                            // $v6 = strlen($t6);
                            // if($v6==9){
                            //     array_push($t, $t6);
                            // }
                            // $t7 = $fields[9];
                            // $v7 = strlen($t7);
                            // if($v7==9){
                            //     array_push($t, $t7);
                            // }
                            $longitud = count($t); 
                            for($j=0; $j<$longitud; $j++){
                                $sql = "INSERT IGNORE INTO $tableNombre(cedente,Rut,Nombre_Completo,Fono,idCarga, id_usuario) VALUES ('".$cedente."','".$rut."','".$nombre."','".$t[$j]."','".$lastId."', '".$id_usuario."')";
                                $rows = $db->query($sql);
                            
                            }
                        }
                        unset($t);
                        $t = array();
                        $i++;
                    }
                    fclose($fp);
                    $sql = "INSERT  INTO SYS_process(process) VALUES ('carga')";
                    $rows = $db->query($sql);
                    $sqlUpdate = "UPDATE CRG_file SET process = 1 WHERE id = '".$idFile."'";
                    $rowsUpdate= $db->query($sqlUpdate);

                    $contarRut = "SELECT Rut FROM $tableNombre WHERE idCarga = $lastId group by Rut";
                    $contarFonos = "SELECT Fono FROM $tableNombre WHERE idCarga = $lastId group by Fono";

                    $rowsRut= $db->select($contarRut);
                    $rowsFono= $db->select($contarFonos);
                    $cantidadRut = count($rowsRut);
                    $cantidadFono = count($rowsFono);
                    $sqlUpdate = "UPDATE BT_config SET cantidad_rut  = '".$cantidadRut."',cantidad_fono  = '".$cantidadFono."' WHERE id = '".$lastId."'";
                    $rowsUpdate= $db->query($sqlUpdate);
                    return 'success';
                }
                else
                {
                    //si el xls da error, borro la tabla que seria para ingresar esos datos
                    $drop = $db->query($dropTabla);
                    
                    //borro el registro de la tabla en BT_config
                    $sqlUpdate = "DELETE FROM BT_config WHERE id = '".$lastId."'";
                    $rowsUpdate= $db->query($sqlUpdate);
                     return $xlsx_mensaje_error = 'xlsx error: '.$xlsx->error();
                     
                }
            }else{
                return "si";
            }

        }
    }    
}
?>    