<?php
class Registros {

    private $db;

    public function __construct() {
        $this->db=Conectar::conexion();
    }
    
    public function getTotalRegistros($tabla)
    {
        
        $comentarios = '';
        $comentarios = " SELECT * FROM $tabla";

        //ejecutamos la consulta
        $consulta = $this->db->query($comentarios);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $totalRows = count($respuesta);
        return $totalRows;
    }

    public function getTotalRegistrosWhere($tabla, $idCarga)
    {
        
        $comentarios = '';
        $comentarios = " SELECT * FROM $tabla where idCarga = $idCarga";

        //ejecutamos la consulta
        $consulta = $this->db->query($comentarios);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $totalRows = count($respuesta);
        return $totalRows;
    }
}
?>