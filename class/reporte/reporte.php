<?php

// include '../db/database_connection.php';

class reporte {
    private $db;

    public function __construct() {
        $this->db=Conectar::conexion();
    }
    // metodo para descargar excel
    public function getExcel() {
        $output = '';
        $fp = fopen("bt_id.txt", "r");
        $idCarga = fgets($fp);
        fclose($fp);
        if($idCarga != '')
        {
       
        $query = "SELECT * FROM RP_resultado_bot WHERE idCarga = $idCarga";

        $result = mysqli_query($this->db, $query);
        if(mysqli_num_rows($result) > 0)
        {
        $output .= '
        <table class="table" bordered="1">  
                            <tr>  
                                <th>ID</th>  
                                <th>RUT</th>  
                                <th>Fono</th>
                                <th>Gestion</th>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>URL Grabacion</th>
                                <th>ID Carga</th>
                                <th>Desvio</th>
                                <th>Respuesta Cliente</th>
                                
                            </tr>
        ';
        while($row = mysqli_fetch_array($result))
        {
        $output .= '
            <tr>  
                                <td>'.$row["id"].'</td>  
                                <td>'.$row["rut"].'</td>  
                                <td>'.$row["fono"].'</td>  
                                <td>'.$row["gestion"].'</td>  
                                <td>'.$row["fecha"].'</td>
                                <td>'.$row["hora"].'</td>
                                <td>'.$row["urlGrabacion"].'</td>
                                <td>'.$row["idCarga"].'</td>
                                <td>'.$row["desvio"].'</td>
                                <td>'.$row["respuesta_cliente"].'</td>
                                

                            </tr>
        ';
        }
        $output .= '</table>';
        header('Content-Type: application/xls');
        header('Content-Disposition: attachment; filename=reporte.xls');
        echo $output;
        }
        }
        else
        {
            echo 'Error idCarga esta vacio';
        }
    }

    public function getIdBT_config () {
        
        // $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        // $data = array();
        // //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        // $filtered_rows = count($respuesta);

        $descripciones = '';
        $sql = "SELECT id, nombre FROM BT_config order by id desc";
            //ejecutamos la consulta
            $result = $this->db->query($sql);
            $cantidad = $result->num_rows;
            if($cantidad > 0){
                $descripciones .= '<option value="Seleccione">Seleccione</option>';
                foreach($result as $rowsD){
                    
                    $descripciones .= '<option class="id" id="'.$rowsD["id"].'" value="'.$rowsD["id"].'">'.$rowsD['nombre'].'</option>';
                }
                
            }else{
                $descripciones = '<option value="">No existe data</option>';
            }
            return $descripciones;
    }
    //metodo para guardar el id de los reportes a consultar y mostrar la data table con ellos
    public function guardagId(){
        
        $bt_id = $_POST['bt_id'];
        $fp = fopen("bt_id.txt", "w");
        fputs($fp, $bt_id);
        fclose($fp);
    }
    // metodo para obtener todos los reportes
    public function getReportes() {
        
        $fp = fopen("bt_id.txt", "r");
        $idCarga = fgets($fp);
        
        fclose($fp);
        
        $query = '';

        $output = array();

        $query .= " SELECT * FROM RP_resultado_bot WHERE idCarga = $idCarga ";
        // $query .= " SELECT * FROM RP_resultado_bot ";
        if(isset($_POST["search"]["value"]))
        {
            // $query .= 'OR id LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= ' OR rut LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= ' or fono LIKE "%'.$_POST["search"]["value"].'%" ';
            $query .= 'and gestion LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR fecha LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR hora LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR urlGrabacion LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR idCarga LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR desvio LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR respuesta_cliente LIKE "%'.$_POST["search"]["value"].'%" ';
            // $query .= 'OR rut2 LIKE "%'.$_POST["search"]["value"].'%" ';
        }
        if(isset($_POST["order"]))
        {
            $query .= 'ORDER by '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].' ';
        }
        else{
            $query .= 'ORDER BY id DESC ';
        }
        if($_POST["length"] != -1)
        {
            $query .= 'LIMIT ' .$_POST['start'] . ', ' . $_POST['length'];
        }

        
        //ejecutamos la consulta
        $consulta = $this->db->query($query);
        $respuesta = $consulta->fetch_all(MYSQLI_ASSOC);
        
        $data = array();
        //uso el metodo cout() para saber si existe al menos 1 elemento en el array
        $filtered_rows = count($respuesta);
        
        foreach($respuesta as $row)
        {
            
            $sub_array = array();
            $sub_array[] = $row['id'];
            $sub_array[] = $row['rut'];
            $sub_array[] = $row['fono'];
            $sub_array[] = $row['gestion'];
            $sub_array[] = $row['fecha'];
            $sub_array[] = $row['hora'];
            $sub_array[] = '<audio src="'.$row['urlGrabacion'].'" controls ></audio>';
            $sub_array[] = $row['idCarga'];
            $sub_array[] = $row['desvio'];
            $sub_array[] = $row['respuesta_cliente'];
            // $sub_array[] = $row['rut2'];
            // $sub_array[] = '<button style="cursor:pointer;" type="button" name="update" id="'.$row["id"].'" class="btn btn-warning btn-xs update">Actualizar</button>';
            // $sub_array[] = '<button type="button" name="cambiar" id="'.$row["id"].'" class="btn btn-danger btn-xs cambiar" data-status="'.$row["cliente"].'">Cambiar Estatus</button>';
            // $sub_array[] = '<button style="cursor:pointer;" type="button" name="borrar" id="'.$row["id"].'" class="btn btn-danger btn-xs borrar">Eliminar Reporte</button>';
            $data[]      = $sub_array;
        }
        $registro = new Registros();
        $tabla = 'RP_resultado_bot';
        $output = array(
            "draw"              => intval($_POST["draw"]),
            "recordsTotal"      => $filtered_rows,
            "recordsFiltered"   => $registro->getTotalRegistrosWhere($tabla, $idCarga),
            "data"              => $data
        );

        return json_encode($output);
    }
    // fin metodo para obtener todos los reportes

}

?>