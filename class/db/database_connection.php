<?php

//database_connection.php

// $connect = new PDO('mysql:host=localhost;dbname=webservice_db', 'root', '');
// $connect = mysqli_connect("localhost", "root", "", "webservice_db");

class Conectar{

    public static function conexion(){
        //coneccion localhost
        $conexion=new mysqli("localhost", "root", "", "maia");

        //conexion al servidor desde localhost
        // $conexion=new mysqli("200.54.39.67", "root", "m9a7r5s3", "maia");
        
        //conexion en el servidor
        //  $conexion=new mysqli("localhost", "root", "m9a7r5s3", "maia");

        $conexion->query("SET NAMES 'utf8'");

        if (mysqli_connect_errno()) {
            printf("Error de conexión: %s\n", mysqli_connect_error());
            exit();
        }
        return $conexion;
    }

}

?>
