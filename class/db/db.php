<?php
   class db {
    public $Server;
    public $User;
    public $Pass;
    public $Database;

    public $Link;

    protected static $connection;
    protected static $connectionDiscador;
    protected static $connectionDiscadorAsterisk;

    /**
     * Connect to the database
     *
     * @return bool false on failure / mysqli MySQLi object instance on success
     */
    public function __construct($Link = "maia"){
        $this->Link = $Link;
        $Conf = parse_ini_file("conf.ini");
        if($Link == "maia"){
            $this->Server = $Conf["serverDB"];
            $this->Pass = $Conf["passDB"];
            $this->User = $Conf["userDB"];
            $this->Database = $Conf["DB"];
        }
        else if($Link == "discador"){
            $this->Server = $Conf["serverDB_discador"];
            $this->Pass = $Conf["passDB_discador"];
            $this->User = $Conf["userDB_discador"];
            $this->Database = $Conf["DB_discador"];
        }
        else if($Link == "asterisk"){
            $this->Server = $Conf["serverDB_discador_asterisk"];
            $this->Pass = $Conf["passDB_discador_asterisk"];
            $this->User = $Conf["userDB_discador_asterisk"];
            $this->Database = $Conf["DB_discador_asterisk"];
            
        }
        if (!isset($_SESSION)){
        }
    }

    public function connect() {
        $ToReturn =  "";
        switch($this->Link){
            case "maia":
                // Try and connect to the database
                if(!isset(self::$connection)) {
                    //self::$connection = mysql_connect($this->Server,$this->User,$this->Pass);
                    self::$connection = mysqli_connect($this->Server,$this->User,$this->Pass);
                    mysqli_select_db(self::$connection, $this->Database);
                }

                // If connection was not successful, handle the error
                if(self::$connection === false) {
                    // Handle error - notify administrator, log to a file, show an error screen, etc.
                    $ToReturn = false;
                }
                $ToReturn = self::$connection;
            break;
            case "discador":
                // Try and connect to the database
                if(!isset(self::$connectionDiscador)) {
                    self::$connectionDiscador = mysqli_connect($this->Server,$this->User,$this->Pass);
                    mysqli_select_db(self::$connectionDiscador, $this->Database);
                }

                // If connection was not successful, handle the error
                if(self::$connectionDiscador === false) {
                    // Handle error - notify administrator, log to a file, show an error screen, etc.
                    $ToReturn = false;
                }
                if($ToReturn !== false){

                }
                $ToReturn = self::$connectionDiscador;
            break;
            case "asterisk":
                // Try and connect to the database
                if(!isset(self::$connectionDiscadorAsterisk)) {
                    self::$connectionDiscadorAsterisk = mysqli_connect($this->Server,$this->User,$this->Pass);
                    mysqli_select_db(self::$connectionDiscadorAsterisk, $this->Database);
                }

                // If connection was not successful, handle the error
                if(self::$connectionDiscadorAsterisk === false) {
                    // Handle error - notify administrator, log to a file, show an error screen, etc.
                    $ToReturn = false;
                }
                if($ToReturn !== false){

                }
                $ToReturn = self::$connectionDiscadorAsterisk;
            break;
        }
        return $ToReturn;
    }

    /**
     * Query the database
     *
     * @param $query The query string
     * @return mixed The result of the mysqli::query() function
     */
    public function query($query) {
        // Connect to the database
        $connection = $this -> connect();
        // Query the database
        $result = mysqli_query($connection, $query);
        if ($result){
            if(isset($_SESSION["id_usuario"])){
                $this->registrarLogSistema($query);
            }
        }else{
            $result = mysqli_error($connection);
            // echo $query;
        }
        return $result;
    }

    public function insert($query){
        $connection = $this->connect();
        if ($connection) {
            $result = mysqli_query($connection, $query);
            if ($result) {
                $return = mysqli_insert_id($connection); 
            }else{
                $return = mysqli_error($connection);
            }
            return $return;
        }else{
            return 'No hay conexion';
        }
    }

    function getErrorMessage(){
        //return $this->Server."/".mysql_error();
        //return $this->Server."/".mysqli_error($link);
    }

    public function select($query) {
        $rows = array();
        $result = $this -> query($query);
        if($result === false) {
            return false;
        }
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }


    public function getLastID(){
        $connection = $this -> connect();
        return mysqli_insert_id($connection);
    }
    
   
}

?>