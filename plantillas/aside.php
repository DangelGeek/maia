<aside class="sidebar">
                <div class="scrollbar-inner">

                    <div class="user">
                        <div class="user__info" data-toggle="dropdown">
                            <!-- <img class="user__img" src="../demo/img/profile-pics/2.jpg" alt=""> -->
                            <div>
                                <div class="user__name"> <?php echo $_SESSION['user_name']; ?></div>
                                <div class="user__email"><?php echo $_SESSION['correo_name']; ?></div>
                            </div>
                        </div>

                        <div class="dropdown-menu">
                            <!-- <a class="dropdown-item" href="">Ver Perfil</a> -->
                            <a class="dropdown-item" href="../perfil">Configurar cuenta</a>
                            <a class="dropdown-item" href="../logout.php">Salir</a>
                        </div>
                    </div>

                    <ul class="navigation">
                        <li id="dashboard" class="@@indexactive"><a href="../"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                        <!-- navigation__sub--active -->
                        <li id="carga" class="navigation__sub @@formactive">
                            <a href="../carga/"><i class="zmdi zmdi-collection-item"></i> Carga</a>
                            <ul>
                                <li id="carga_subir" class="@@formelementactive "><a href="../carga/">Subir</a></li>
                                <li id="carga_ver" class="@@formelementactive "><a href="../carga/verCargas.php">Ver Cargas</a></li>
                            </ul>
                        </li>

                        <li id="reporte" class="navigation__sub @@uiactive">
                            <a href="../reporte/"><i class="zmdi zmdi-trending-up"></i> Reporte</a>
                            <ul>
                                <li id="reporte_bot" class="@@colorsactive"><a href="../reporte/">Reporte Bot</a></li>
                            </ul>
                        </li>

                        <li id="gestion" class="navigation__sub @@componentsactive">
                            <a href="../gestion/"><i class="zmdi zmdi-collection-text"></i>Gestión</a>
                            <ul class="navigation__sub">
                                <li id="gestion_bot" class="@@carouselactive"><a href="../gestion/">Bot</a></li>
                            </ul>
                        </li>
                       <?php
                       if($_SESSION['type'] == 'master')
                       {
                        ?>
                        <li id="configuracion" class="navigation__sub @@chartsactive">
                            <a href="../perfiles/"><i class="zmdi zmdi-accounts-outline"></i>Configuración</a>
                            <ul>
                                <li id="configuracion_perfil" class="@@flotchartsactive"><a href="../perfiles/">Perfiles</a></li>
                            </ul>
                        </li>
                    <?php
                        }
                    ?>
                       
                       
                        
                    </ul>
                    
                    
                    
                </div>
            </aside>