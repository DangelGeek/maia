var fs = require('fs');
var ini = require('ini');

var configIni = ini.parse(fs.readFileSync('../class/db/conf.ini', 'utf-8')); //RUTA WINDOWS
//var configIni = ini.parse(fs.readFileSync('/var/www/html/class/db/conf.ini', 'utf-8')); //RUTA LINUX

configIni = configIni.maia;

//var sql = require("mssql");
var mysql = require('mysql');
// config for your database

var Host = "";
var Port = "";

if(configIni.serverDB.indexOf(':') != -1){
    Host = configIni.serverDB.substring(0, configIni.serverDB.indexOf(':'));
    Port = configIni.serverDB.substring(configIni.serverDB.indexOf(':') + 1,9999);
}else{
    Port = "3306";
}

var db = mysql.createConnection({
    host: Host,
    port: Port,
    user: configIni.userDB,
    password: configIni.passDB,
    database: configIni.DB
});


var ArraySocketsUsuarios = new Object();
var ArrayUsuariosSockets = new Object();

db.connect(function (err) {
    if (err) {
        console.log("Error con credenciales de Base de datos.",err);
    } else {
        console.log("Conectado a MAIA...");
        var app = require('express')();
        var http = require('http').Server(app);
        var io = require('socket.io')(http);
        var port = process.env.PORT || configIni.portNode;

        http.listen(port);

        require('events').EventEmitter.defaultMaxListeners = Infinity;
        
        io.on('connection', function (socket) {
            var idSocket = socket.id;

            socket.on("createLogin", function (Data) {
                var idUsuario = Data.idUsuario;
                var idCedente = Data.idCedente;
                var idMandante = Data.idMandante;
                db.query("INSERT INTO SYS_users_actives (idUser,idSocket) values ('" + idUsuario + "','" + idSocket + "')", function (err, recordset) {
                    if (!err) {
                        recordset = JSON.parse(JSON.stringify(recordset));
                        var idLogin = recordset.insertId;
                        io.to(idSocket).emit("loginResponse", { insertID: idLogin });
                        ArraySocketsUsuarios[idLogin] = idSocket;
                        ArrayUsuariosSockets[idSocket] = idLogin;
                        console.log(ArraySocketsUsuarios);
                        console.log(ArrayUsuariosSockets);
                    }
                });
            });
    
            socket.on('disconnect', function (Data) {
                var idLogin = ArrayUsuariosSockets[idSocket];
                db.query("DELETE FROM SYS_users_actives where id='"+idLogin+"'", function (err, recordset) {
                    if (!err) {
                        recordset = JSON.parse(JSON.stringify(recordset));
                        delete ArraySocketsUsuarios[idLogin];
                        delete ArrayUsuariosSockets[idSocket];
                    }
                });
            });

            socket.on("listenerCarga",function(Data){
                db.query("SELECT * FROM SYS_process WHERE process = 'carga'", function (err, recordset) {
                    recordset = JSON.parse(JSON.stringify(recordset));
                    if (!err){
                        var Rows = recordset;
                        if(Rows.length > 0){
                            io.to(idSocket).emit("responseCarga", { insertID: recordset });
                            db.query("DELETE FROM SYS_process WHERE process = 'carga'", function (err, recordset) {});
                        }
                    }
                });
            });
        });
    }
});


