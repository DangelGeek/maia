
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Vendor styles -->
        <link rel="stylesheet" href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="vendors/bower_components/sweetalert2/dist/sweetalert2.min.css">
        <link rel="stylesheet" href="vendors/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- App styles -->
        <link rel="stylesheet" href="css/app.css">
    </head>

    <body data-sa-theme="1">
        <div class="login">

            <!-- Login -->
            <div class="container text-center">
            <div class="row">
                <div class="col-md-12">
                <form class="login-wrapper" id="form_login" method="post">
                    <div class="login__block active" id="l-login">
                        <div class="login__block__header">
                            <i class="zmdi zmdi-account-circle"></i>
                            Acceso 

                        </div>

                    <div class="login__block__body">
                        <div class="form-group">
                            <input type="text" class="form-control text-center" name="userName" placeholder="Usuario">
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control text-center" placeholder="Password" name="Password1">
                        </div>

                        <button class=" btn btn--icon login__block__btn" id="login" name="login" type="submit" value="Entrar">
                            <i class="zmdi zmdi-long-arrow-right "></i>
                        </button>
                        <input type="hidden" name="login">
                        <!-- <a href="#" class="acceso btn btnicon login__block__btn "><i class="zmdi zmdi-long-arrow-right "></i></a> -->
                            </div>
                        </div>
                        <div class="show" id="show">
                            <div id="view" ></div>
                        </div>
                    </div>
                    
                </form>
                
                </div>
                

            </div>
                

            </div>
            
        
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
        <script src="vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
        <script src="vendors/bower_components/jqueryvalidate/jqueryvalidate.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script> -->
        <script src="js/controllers/controllerLogin/controllerLogin.js"></script>
        <!-- App functions and actions -->
        <!-- <script src="js/app.min.js"></script> -->
        <!-- <script src="js/login.js"></script> -->
    </body>
</html>