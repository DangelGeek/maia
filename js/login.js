$(document).ready(function(){
    $('.acceso').click(function(){
        var username = $("input[name=username]").val();
        var password = $("input[name=password]").val();
        var data = 'username='+username+"&password="+password;
        $.ajax({
            type: "POST",
            url: "includes/login/login.php",
            data:data,
            success: function(response){
                console.log(response);
                if(response==1){
                    window.location.replace("dashboard/index.php");

                }else{
                    swal({
                        title: 'Acceso Inválido',
                        text: 'Compruebe su nombre de usuario o contraseña',
                        type: 'warning',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-sm btn-light',
                        background: 'rgba(0, 0, 0, 0.96)'
                    });
                }
            }
        });  
    });
});