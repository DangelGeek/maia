$(document).ready(function(){

    $.ajax({
        type: "GET",
        url: "../includes/carga/descripcion.php",
        success: function(data){
            $('#bt_descipcion').append(data);
        }
    });
    
    subirExcel();
    function subirExcel(){
        Dropzone.options.dropzoneFrom = {
            autoProcessQueue: false,
            acceptedFiles:".xls,.xlsx",
            addRemoveLinks: true,
            
            init: function(){
                var submitButton = document.querySelector('#subir');
                myDropzone = this;
                    
                submitButton.addEventListener("click", function(){
                    myDropzone.processQueue();
                    if(myDropzone.files.length){
                        console.log('archivo en cola');
                    }
                    else{
                        swal({
                            title:'Error!',
                            text: 'No existe archivo en cola',
                            type: 'warning',
                            background: 'rgba(0, 0, 0, 0.96)'
                        });
                    }
                    

                });
                
                this.on("sending", function(file, xhr, formData) {
                    
                    console.log(data);
                    
                    formData.append("nombreCarga", nombreCarga);
                    formData.append("bt_id", bt_id);
                    formData.append("servicio", bt_id);
                    formData.append("id_usuario", id_usuario);
                 });
                this.on("complete", function(){
                var _this = this;
                setTimeout(function(){
                    
                    _this.removeAllFiles();
                }, 4000);
                });
                /* On Success, do whatever you want */
                this.on("success", function(file, responseText) {
                var respuesta = $.parseJSON(responseText);
                    if(respuesta)
                    {
                        setTimeout(function(){
                                    swal({
                                    title:respuesta.mensaje,
                                    text: respuesta.texto,
                                    type: respuesta.tipo,
                                    background: 'rgba(0, 0, 0, 0.96)'
                                });
                            }, 3000);
                            $("input[name=nombreCarga]").val("");
                             $("select").val('').change();
                            return;
                    }
                });
                
            },
        };
    };

    $('#subir').click(function(){
        nombreCarga = $("input[name=nombreCarga]").val();
        servicio = $("#servicio").val();
        //obtener id del bot
        bt_id = $("#bt_descipcion").val();
        id_usuario = $("#id_usuario").val();
        data = "nombreCarga="+nombreCarga+"&servicio="+bt_id+"&bt_id="+bt_id+"&id_usuario="+id_usuario;
        
        if(nombreCarga==''){
            swal({
                title: 'Error',
                text: 'Debe Darle un nombre a la Carga',
                type: 'warning',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-sm btn-light',
                background: 'rgba(0, 0, 0, 0.96)'
            }); 
        }else{
            if(servicio==''){
                swal({
                    title: 'Error',
                    text: 'Debe Seleccionar un Servicio!',
                    type: 'warning',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                });
            }else
                if(bt_id=='Seleccione'){
                    swal({
                        title: 'Error',
                        text: 'Debe Seleccionar un BOT!',
                        type: 'warning',
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-sm btn-light',
                        background: 'rgba(0, 0, 0, 0.96)'
                    });
                }
            //     else{
            // }
        }
    });
});