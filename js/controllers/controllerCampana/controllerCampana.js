

$(document).ready(function(){

    $("#user_form2").validate({
        rules         : {
            id_user         : { required : true, minlength: 1},
            campana_name2      : { required : true, minlength: 2},
            rut             : { required : true, minlength: 1},
            user_name2      : { required : true, minlength: 2},
            user_tel        : { required : true, minlength: 7},
            user_deuda      : { required:true, minlength: 2}
        },
        messages      : {
            id_user      : "Debe Seleccionar un id de usuario.",
            campana_name2      : "Debe introducir un nombre de Campaña.",
            rut                : "Debe introducir un rut.",
            user_name2         : "Debe introducir un Nombre de usuario.",
            user_tel           : "Debe introducir un Télefono válido.",
            user_deuda         : "El campo Deuda es obligatorio."
            
        }
    });
    console.log('funciona controllerCampana');
    //subir data para una campaña como user o master
    $('#form_campana_data').on('submit', function(event){
        event.preventDefault();  
        $('#btn_submit').attr('disabled','disabled');
        dataform = new FormData(this);
        console.log('la data es: '+dataform);
        $.ajax({
              url:"../controllers/controllerCampana/controllerCampana.php",
              method:"POST",  
              data:new FormData(this),
              contentType:false,  
              processData:false,  
              success:function(data){  
                //   $('#result').html(data).show(2000).delay(3000).hide(5000);
                  $('#result').html(data);
                  $('#form_campana').show(2000);
                  $('#file_crear_campana').val('');
                  $('#btn_submit').attr('disabled', false);
                //   userdataTable.ajax.reload();
                //   resfreshArbol();
                //   VerificarData();
              }
        });
    });
});