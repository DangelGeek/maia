//muestra todos los datos de la tabla reportes
$(document).ready(function() {

    // Add custom buttons
    var dataTableButtons =  '<div class="dataTables_buttons  actions">' +
                                '<span class="actions__item zmdi zmdi-accounts-add" name="add" id="add_button" data-toggle="modal" data-target="#userModal"/>' +
                                '<span class="actions__item zmdi zmdi-print" data-table-action="print" />' +
                                '<span class="actions__item zmdi zmdi-fullscreen" data-table-action="fullscreen" />' +
                                '<div class="dropdown actions__item">' +
                                    '<i data-toggle="dropdown" class="zmdi zmdi-download" />' +
                                    '<ul class="dropdown-menu dropdown-menu-right">' +
                                        '<a href="" class="dropdown-item" data-table-action="excel">Excel (.xlsx)</a>' +
                                        '<a href="" class="dropdown-item" data-table-action="csv">CSV (.csv)</a>' +
                                    '</ul>' +
                                '</div>' +
                            '</div>';

    var userdataTable = $('#user_data').DataTable({
        autoWidth: false,
        responsive: false,
        "processing": true,
        "serverSide": true,
        lengthMenu: [[5, 10, 20, -1], ['5 Filas', '10 Filas', '20 Filas', 'Todas']], //Length select
        language: {
            searchPlaceholder: "Buscar Usuarios...", // Search placeholder
            processing:     "Busqueda en Curso...",
            info:           "Mostrando _START_  a _END_ de _TOTAL_ entradas",
            infoFiltered:   "(filtrado de _MAX_ entradas totales)",
            emptyTable:     "No hay datos disponibles en la tabla",
            infoEmpty:      "No hay registros disponibles",
        },
        dom: 'Blfrtip',
        buttons: [ // Data table buttons for export and print
            {
                extend: 'excelHtml5',
                title: 'Reporte Bot Excel'
            },
            {
                extend: 'csvHtml5',
                title: 'Reporte Bot CSV'
            },
            {
                extend: 'print',
                title: 'Reporte Bot PDF'
            }
        ],
        "order": [],
        "ajax":{
          url:"../includes/edit_perfil/get_perfil.php",
          type:"POST"
        },
        "columnDefs":[
          {
            "target":[4,7], //numero de columnas que se muestran, contadas desde 0
            "orderable":false
          }
        ],
        "pageLength":5, //maximo filas a mostrar en una vista
        "initComplete": function(settings, json) {
            $(this).closest('.dataTables_wrapper').prepend(dataTableButtons); // Add custom button (fullscreen, print and export)
        }
    });

    // Data table button actions
    $('body').on('click', '[data-table-action]', function (e) {
        e.preventDefault();

        var exportFormat = $(this).data('table-action');

        if(exportFormat === 'excel') {
            $(this).closest('.dataTables_wrapper').find('.buttons-excel').trigger('click');
        }
        if(exportFormat === 'csv') {
            $(this).closest('.dataTables_wrapper').find('.buttons-csv').trigger('click');
        }
        if(exportFormat === 'print') {
            $(this).closest('.dataTables_wrapper').find('.buttons-print').trigger('click');
        }
        if(exportFormat === 'fullscreen') {
            var parentCard = $(this).closest('.card');

            if(parentCard.hasClass('card--fullscreen')) {
                parentCard.removeClass('card--fullscreen');
                $('body').removeClass('data-table-toggled');
            }
            else {
                parentCard.addClass('card--fullscreen')
                $('body').addClass('data-table-toggled');
            }
        }
    });

    // cuando enviamos la data del formulario de usuario en el modal, por defecto agrega
  $(document).on('submit', '#user_form', function(event){
    event.preventDefault();
    if($('#user_new_password2').val() != $('#user_re_enter_password2').val())
    {
    // $('#error_password2').fadeIn(1000).html('<div style="margin-top:20px;" class="alert alert-danger text-danger">Las Contraseñas no son iguales</div>').delay(1000).fadeOut(3000);
    notificaciones('Las Contraseñas no son iguales', 'danger');
    return false;
    }
    $('#action').attr('disabled','disabled');
    var form_data = $(this).serialize();
    var tipo_user = $('#user_type').val();
    if(tipo_user != 'master' && tipo_user != 'user' )
    {
        $('#alert_user_type').fadeIn(1000).html('<div class="alert alert-danger">Solo se permite tipo de usuario master o user</div>').delay(1000).fadeOut(3000);
        return;
    }
    $.ajax({
     url:"../includes/edit_perfil/edit_perfil.php",
     method:"POST",
     data:form_data,
     success:function(data)
     {
       //el formulario esta enlazado con el evento click en la clase .update, cambiamos los valores de los inputos luego de editar a Add
      $('#btn_action').val('');
      $('#action').val('Add');
      $('.modal-title').html("<i class='fa fa-plus'></i> Agregar Usuario");
      $('#user_form')[0].reset();
      $('#userModal').modal('hide');
    //   $('#alert_action').fadeIn(1000).html('<div class="alert alert-success">'+data+'</div>').delay(1000).fadeOut(3000);
      notificaciones(data, 'success');
      $('#action').attr('disabled', false);
      userdataTable.ajax.reload();
     }
    })
   });

   // edita un usuario
  $(document).on('click', '.update', function(){
    var user_id = $(this).attr("id");
    var btn_action = 'fetch_single';
    $.ajax({
      url:"../includes/edit_perfil/edit_perfil.php",
      method:"POST",
      data:{
        user_id:user_id,
        btn_action:btn_action
      },
      dataType:"json",
      success:function(data)
      {
        //retorno la data en json, muestro el modal y los datos json en los inputs para editarlos
        
        $('#userModal').modal('show');
        $('#user_name2').val(data.user_name);
        $('#correo_name').val(data.user_email);
        $('#user_new_password2').val('');
        $('#user_re_enter_password2').val('');
        $('#user_type').val(data.user_type);
        $('#estatus').val(data.user_estatus);
        $('.modal-title').html("<i class='fa fa-pencil-square-o'></i>Editar Usuario");
        $('#user_id').val(user_id);
        $('#action').val('Edit');
        $('#btn_action').val('Edit');
      }
    });
  });

  $(document).on('click', '.delete', function(){
    var user_id = $(this).attr("id");
    var status = $(this).data('status');
    var btn_action = "delete";
    if(confirm("¿Seguro quieres cambiar el estatus?")){
            $.ajax({
                url:"../includes/edit_perfil/edit_perfil.php",
                method:"POST",
                data:{user_id:user_id, status:status,
                    btn_action:btn_action},
                success:function(data){
                    // $("#alert_action").fadeIn(1000).html('<div class="alert alert-info">'+data+'</div>').delay(1000).fadeOut(3000);
                    notificaciones(data, 'info');
                    userdataTable.ajax.reload();
                }
            })
    } else {
        return false;
    }
});

    $(document).on('click', '.borrar', function(){
    var user_id = $(this).attr("id");
    var btn_action = "borrar";
    if(confirm("¿Seguro quieres Eliminar el Contacto?")){
            $.ajax({
                url:"../includes/edit_perfil/edit_perfil.php",
                method:"POST",
                data:{user_id:user_id,
                    btn_action:btn_action},
                success:function(data){
                    // $("#alert_action").fadeIn(1000).html('<div class="alert alert-info">'+data+'</div>').delay(1000).fadeOut(3000);
                    notificaciones(data, 'danger');
                    userdataTable.ajax.reload();
                }
            })
    } else {
        return false;
    }
    });

    // funcion para mostrar notificacion de botones
    function notificaciones(mensaje, tipo){
        $.notify({
            message: mensaje
        },{
            type: tipo, // 'inverse', 'info', 'success', or 'danger'
            placement: {
                from: 'top', // 'top' or 'bottom'
                align: 'right' // 'left', 'center' or 'right',
            },
            allow_dismiss: true,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            template:   '<div data-notify="container" class="alert alert-dismissible alert-{0} alert--notify" role="alert">' +
                            '<span data-notify="icon"></span> ' +
                            '<span data-notify="title">{1}</span> ' +
                            '<span class="text-center" style="color:white; font-size:15px; font-weight:bold;" data-notify="message">{2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
                            '<a href="{3}" target="{4}" data-notify="url"></a>' +
                            '<button type="button" aria-hidden="true" data-notify="dismiss" class="close"><span>×</span></button>' +
                        '</div>'
        });
    }


});