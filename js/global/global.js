
$(document).ready(function($){
	
	maiaSocket = io.connect("http://200.54.39.66:65520");
	var idUsuario = 1;
	console.log("Ejecutando node");
	loginSocket();

	function loginSocket(){
		maiaSocket.emit('createLogin', { idUsuario: idUsuario})
		console.log("Enviando Emit");
	}
	maiaSocket.on("loginResponse", function (Data) {
		console.log(Data);
	});
	maiaSocket.on("responseCarga", function (Data) {
		swal({
			title: 'Carga Realizada!',
			text: 'Carga Realizada exitosamente',
			type: 'success',
			buttonsStyling: false,
			confirmButtonClass: 'btn btn-sm btn-light',
			background: 'rgba(0, 0, 0, 0.96)'
		});
		setTimeout(function() {window.location.replace("subir.php");}, 4000);

	});

	//INTERVALOS DE ALERTAS
	var alertCarga;

	function myInterval() {
		alertCarga = setInterval(getCarga, 5000);
	}

	function getCarga(){
		maiaSocket.emit('listenerCarga', { idUsuario: idUsuario})
		console.log("Enviando Emit");
	}
	myInterval();

});
