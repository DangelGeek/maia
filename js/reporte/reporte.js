//muestra todos los datos de la tabla reportes
$(document).ready(function() {
    $('#tabla_reporte').hide();
    //obtener nombres de reportes
    $.ajax({
        type: "GET",
        url: "../includes/reporte/getIdBT_config.php",
        success: function(data){
            $('#select_nombre').append(data);
        }
    });

    $('#consultar').click(function(){
        
        //obtener id de la carga
        var bt_id = $("#select_nombre").val();
        
        var data = "bt_id="+bt_id;
        console.log(data);
        if(bt_id=='Seleccione'){
            swal({
                title: 'Error',
                text: 'Debe Seleccionar un Nombre!',
                type: 'warning',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-sm btn-light',
                background: 'rgba(0, 0, 0, 0.96)'
            });
        }else{
        $.ajax({
            type: "POST",
            url: "../includes/reporte/guardaId.php",
            data:data,
            success: function(data){
                swal({
                    title: 'Consultando Reportes',
                    text: 'Se Mostraran los datos en la tabla cuando el proceso Termine.',
                    type: 'warning',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-sm btn-light',
                    background: 'rgba(0, 0, 0, 0.96)'
                });
                // inicio datatable
                $('#tabla_reporte').show(2000);
                var userdataTable = $('#reporte_data').DataTable({
                    destroy: true,
                    autoWidth: false,
                    responsive: false,
                    "processing": true,
                    "serverSide": true,
                    lengthMenu: [[5, 10, 20, 50], ['5 Filas', '10 Filas', '20 Filas', '50 Filas']], //Length select
                    language: {
                        searchPlaceholder: "Buscar registros por Gestión...", // Search placeholder
                        processing:     "Busqueda en Curso...",
                        info:           "Mostrando _START_  a _END_ de _TOTAL_ entradas",
                        infoFiltered:   "(filtrado de _MAX_ entradas totales)",
                        emptyTable:     "No hay datos disponibles en la tabla",
                        infoEmpty:      "No hay registros disponibles",
                    },
                    dom: 'Blfrtip',
                    buttons: [ // Data table buttons for export and print
                        {
                            extend: 'excelHtml5',
                            title: 'Reporte Bot Excel'
                        },
                        {
                            extend: 'csvHtml5',
                            title: 'Reporte Bot CSV'
                        },
                        {
                            extend: 'print',
                            title: 'Reporte Bot PDF'
                        }
                    ],
                    "order": [],
                    "ajax":{
                    url:"../includes/reporte/reporte.php",
                    type:"POST"
                    },
                    "columnDefs":[
                    {
                        "target":[4,9], //numero de columnas que se muestran, contadas desde 0
                        "orderable":false
                    }
                    ],
                    "pageLength":5, //maximo filas a mostrar en una vista
                    "initComplete": function(settings, json) {
                    $(this).closest('.dataTables_wrapper').prepend(dataTableButtons); // Add custom button (fullscreen, print and export)
                }
                });
                // fin datatable
            }
        });
    }
});

    
  // Add custom buttons
  var dataTableButtons =  '<div class="dataTables_buttons hidden-sm-down actions">' +
                        //   '<span class="actions__item zmdi zmdi-print" data-table-action="print" />' +
                          '<span class="actions__item zmdi zmdi-fullscreen" data-table-action="fullscreen" />' +
                          '<div class="dropdown actions__item">' +
                              '<i data-toggle="dropdown" class="zmdi zmdi-download" />' +
                              '<ul class="dropdown-menu dropdown-menu-right">' +
                                  '<a href="../includes/reporte/excel.php" class="dropdown-item" name="exportar_reporte">Excel (.xls)</a>'
                                //  + '<a href="" class="dropdown-item" data-table-action="csv">CSV (.csv)</a>' +
                              '</ul>' +
                          '</div>' +
                        '</div>';


    // Data table button actions
    $('body').on('click', '[data-table-action]', function (e) {
      e.preventDefault();

      var exportFormat = $(this).data('table-action');

      if(exportFormat === 'excel') {
          $(this).closest('.dataTables_wrapper').find('.buttons-excel').trigger('click');
      }
      if(exportFormat === 'csv') {
          $(this).closest('.dataTables_wrapper').find('.buttons-csv').trigger('click');
      }
      if(exportFormat === 'print') {
          $(this).closest('.dataTables_wrapper').find('.buttons-print').trigger('click');
      }
      if(exportFormat === 'fullscreen') {
          var parentCard = $(this).closest('.card');

          if(parentCard.hasClass('card--fullscreen')) {
              parentCard.removeClass('card--fullscreen');
              $('body').removeClass('data-table-toggled');
          }
          else {
              parentCard.addClass('card--fullscreen')
              $('body').addClass('data-table-toggled');
          }
      }
  });



});