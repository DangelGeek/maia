//muestra todos los datos de la tabla reportes
$(document).ready(function() {

    // Add custom buttons
    var dataTableButtons =  '<div class="dataTables_buttons hidden-sm-down actions">' +
                                // '<span class="actions__item zmdi zmdi-print" data-table-action="print" />' +
                                '<span class="actions__item zmdi zmdi-fullscreen" data-table-action="fullscreen" />' +
                                '<div class="dropdown actions__item">' +
                                    '<i data-toggle="dropdown" class="zmdi zmdi-download" />' +
                                    '<ul class="dropdown-menu dropdown-menu-right">' +
                                        '<a href="" class="dropdown-item" data-table-action="excel">Excel (.xlsx)</a>' +
                                        '<a href="" class="dropdown-item" data-table-action="csv">CSV (.csv)</a>' +
                                    '</ul>' +
                                '</div>' +
                            '</div>';

    var userdataTable = $('#bot_data').DataTable({
        autoWidth: false,
        responsive: false,
        "processing": true,
        "serverSide": true,
        lengthMenu: [[5, 10, 20, -1], ['5 Filas', '10 Filas', '20 Filas', 'Todas']], //Length select
        language: {
            searchPlaceholder: "Buscar registros...", // Search placeholder
            processing:     "Busqueda en Curso...",
            info:           "Mostrando _START_  a _END_ de _TOTAL_ entradas",
            infoFiltered:   "(filtrado de _MAX_ entradas totales)",
            emptyTable:     "No hay datos disponibles en la tabla",
            infoEmpty:      "No hay registros disponibles",
        },
        dom: 'Blfrtip',
        buttons: [ // Data table buttons for export and print
            {
                extend: 'excelHtml5',
                title: 'Reporte Bot Excel'
            },
            {
                extend: 'csvHtml5',
                title: 'Reporte Bot CSV'
            },
            {
                extend: 'print',
                title: 'Reporte Bot PDF'
            }
        ],
        "order": [],
        "ajax":{
          url:"../includes/gestion/bot.php",
          type:"POST"
        },
        "columnDefs":[
          {
            "target":[4,8], //numero de columnas que se muestran, contadas desde 0
            "orderable":false
          }
        ],
        "pageLength":5, //maximo filas a mostrar en una vista
        "initComplete": function(settings, json) {
            $(this).closest('.dataTables_wrapper').prepend(dataTableButtons); // Add custom button (fullscreen, print and export)
        }
    });

    // Data table button actions
    $('body').on('click', '[data-table-action]', function (e) {
        e.preventDefault();

        var exportFormat = $(this).data('table-action');

        if(exportFormat === 'excel') {
            $(this).closest('.dataTables_wrapper').find('.buttons-excel').trigger('click');
        }
        if(exportFormat === 'csv') {
            $(this).closest('.dataTables_wrapper').find('.buttons-csv').trigger('click');
        }
        if(exportFormat === 'print') {
            $(this).closest('.dataTables_wrapper').find('.buttons-print').trigger('click');
        }
        if(exportFormat === 'fullscreen') {
            var parentCard = $(this).closest('.card');

            if(parentCard.hasClass('card--fullscreen')) {
                parentCard.removeClass('card--fullscreen');
                $('body').removeClass('data-table-toggled');
            }
            else {
                parentCard.addClass('card--fullscreen')
                $('body').addClass('data-table-toggled');
            }
        }
    });
    
    recargarTabla();
    //recargar la tabla cad 5 segunds
    function recargarTabla () {
        setInterval(function(){
            userdataTable.ajax.reload();
        }, 10000);    
    }
    //cambiar estatus a play
    $(document).on('click', '.1', function(){
      var user_id = $(this).attr("id");
      var status = $(this).data('status');
      if(status != 1)
      {
        var btn_action = "play";
        swal({
            title: 'Desea activar la campaña?',
            text: "Presione Si de lo contrario Cancel",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si!',
            background: 'rgba(0, 0, 0, 0.96)'
          }).then((result) => {
            if (result) {
                $.ajax({
                    url:"../includes/gestion/bot_estatus.php",
                    method:"POST",
                    data:{user_id:user_id, status:status,
                        btn_action:btn_action},
                    success:function(data){
                        // $("#alert_action").fadeIn(1000).html('<div class="alert alert-success" role="alert">'+data+'</div>').delay(1000).fadeOut(3000);
                        console.log(data);
                        swal({
                            title:'Estado cambiado a Play!',
                            text: 'El estado ha cambiado.',
                            type: 'success',
                            background: 'rgba(0, 0, 0, 0.96)'
                        })
                        // notificaciones(data, 'success');
                        userdataTable.ajax.reload();
                    }
                });
            }
          })
        
      }
      else
      return;
              
    });

    //cambiar estatus a pause
    $(document).on('click', '.2', function(){
        var user_id = $(this).attr("id");
        var status = $(this).data('status');
        if(status != 2)
        {
            var btn_action = "pause";
            swal({
                title: 'Desea Pausar la campaña?',
                text: "Presione Si de lo contrario Cancel",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si!',
                background: 'rgba(0, 0, 0, 0.96)'
              }).then((result) => {
                if (result) {
                    $.ajax({
                        url:"../includes/gestion/bot_estatus.php",
                        method:"POST",
                        data:{user_id:user_id, status:status,
                            btn_action:btn_action},
                        success:function(data){
                            // $("#alert_action").fadeIn(1000).html('<div class="alert alert-warning">'+data+'</div>').delay(1000).fadeOut(3000);
                            // notificaciones(data, 'warning');
                            userdataTable.ajax.reload();
                        }
                    });
                    swal({
                        title:'Estado cambiado a Pause!',
                        text: 'El estado ha cambiado.',
                        type: 'success',
                        background: 'rgba(0, 0, 0, 0.96)'
                    })
                }
              })
            
            
        }
        else
        return;
                
      });

      //cambiar estatus a stop
    $(document).on('click', '.0', function(){
        var user_id = $(this).attr("id");
        var status = $(this).data('status');
        if(status != 0)
        {
            var btn_action = "stop";
            swal({
                title: 'Desea parar la campaña?',
                text: "Presion si, de lo contrario Cancel",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si!',
                background: 'rgba(0, 0, 0, 0.96)'
              }).then((result) => {
                if (result) {
                    $.ajax({
                        url:"../includes/gestion/bot_estatus_stop.php",
                        method:"POST",
                        data:{user_id:user_id, status:status,
                            btn_action:btn_action},
                        success:function(data){
                            // $("#alert_action").fadeIn(1000).html('<div class="alert alert-danger">'+data+'</div>').delay(1000).fadeOut(3000);
                            // notificaciones(data, 'danger');
                            userdataTable.ajax.reload();
                        }
                    });
                    swal({
                        title:'Estado cambiado a Stop!',
                        text: 'El estado ha cambiado.',
                        type: 'success',
                        background: 'rgba(0, 0, 0, 0.96)'
                    })
                }
              })
            
        }
        else
        return;
    });

    $(document).on('click', '.borrar', function(){
        var user_id = $(this).attr("id");
        var btn_action = "borrar";
        if(confirm("¿Seguro quieres Eliminar los datos?")){
                $.ajax({
                    url:"../includes/gestion/bot_estatus.php",
                    method:"POST",
                    data:{user_id:user_id,
                        btn_action:btn_action},
                    success:function(data){
                        // $("#alert_action").fadeIn(1000).html('<div class="alert alert-info">'+data+'</div>').delay(1000).fadeOut(3000);
                        notificaciones(data, 'danger');
                        userdataTable.ajax.reload();
                    }
                })
        } else {
            return false;
        }
        });

    // funcion para mostrar notificacion de botones
    function notificaciones(mensaje, tipo){
        $.notify({
            message: mensaje
        },{
            type: tipo, // 'inverse', 'info', 'success', or 'danger'
            placement: {
                from: 'top', // 'top' or 'bottom'
                align: 'right' // 'left', 'center' or 'right',
            },
            allow_dismiss: true,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            template:   '<div data-notify="container" class="alert alert-dismissible alert-{0} alert--notify" role="alert">' +
                            '<span data-notify="icon"></span> ' +
                            '<span data-notify="title">{1}</span> ' +
                            '<span class="text-center" style="color:white; font-size:15px; font-weight:bold;" data-notify="message">{2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
                            '<a href="{3}" target="{4}" data-notify="url"></a>' +
                            '<button type="button" aria-hidden="true" data-notify="dismiss" class="close"><span>×</span></button>' +
                        '</div>'
        });
    }


});